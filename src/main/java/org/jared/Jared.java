package org.jared;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.concurrent.ThreadLocalRandom;

import org.jared.utilz.*;

public class Jared {
	static {
		arrayTowns = new String[] { "Aladağ", "Ceyhan", "Çukurova", "Feke", "İmamoğlu", "Karaisalı", "Karataş", "Kozan",
				"Pozantı", "Saimbeyli", "Sarıçam", "Seyhan", "Tufanbeyli", "Yumurtalık", "Yüreğir", "Adıyaman", "Besni",
				"Çelikhan", "Gerger", "Gölbaşı", "Kâhta", "Samsat", "Sincik", "Tut", "Afyonkarahisar", "Başmakçı",
				"Bayat", "Bolvadin", "Çay", "Çobanlar", "Dazkırı", "Dinar", "Emirdağ", "Evciler", "Hocalar", "İhsaniye",
				"İscehisar", "Kızılören", "Sandıklı", "Sinanpaşa", "Sultandağı", "Şuhut", "Ağrı", "Diyadin",
				"Doğubayazıt", "Eleşkirt", "Hamur", "Patnos", "Taşlıçay", "Tutak", "Ağaçören", "Aksaray", "Eskil",
				"Gülağaç", "Güzelyurt", "Ortaköy", "Sarıyahşi", "Amasya", "Göynücek", "Gümüşhacıköy", "Hamamözü",
				"Merzifon", "Suluova", "Taşova", "Akyurt", "Altındağ", "Ayaş", "Balâ", "Beypazarı", "Çamlıdere",
				"Çankaya", "Çubuk", "Elmadağ", "Etimesgut", "Evren", "Gölbaşı", "Güdül", "Haymana", "Kalecik",
				"Kahramankazan", "Keçiören", "Kızılcahamam", "Mamak", "Nallıhan", "Polatlı", "Pursaklar", "Sincan",
				"Şereflikoçhisar", "Yenimahalle", "Akseki", "Aksu", "Alanya", "Döşemealtı", "Elmalı", "Finike",
				"Gazipaşa", "Gündoğmuş", "İbradı", "Demre", "Kaş", "Kemer", "Kepez", "Konyaaltı", "Korkuteli",
				"Kumluca", "Manavgat", "Muratpaşa", "Serik", "Ardahan", "Çıldır", "Damal", "Göle", "Hanak", "Posof",
				"Ardanuç", "Arhavi", "Artvin", "Borçka", "Hopa", "Murgul", "Şavşat", "Yusufeli", "Bozdoğan",
				"Buharkent", "Çine", "Didim", "Efeler", "Germencik", "İncirliova", "Karacasu", "Karpuzlu", "Koçarlı",
				"Köşk", "Kuşadası", "Kuyucak", "Nazilli", "Söke", "Sultanhisar", "Yenipazar", "Altıeylül", "Ayvalık",
				"Balya", "Bandırma", "Bigadiç", "Burhaniye", "Dursunbey", "Edremit", "Erdek", "Gömeç", "Gönen",
				"Havran", "İvrindi", "Karesi", "Kepsut", "Manyas", "Marmara", "Savaştepe", "Sındırgı", "Susurluk",
				"Amasra", "Bartın", "Kurucaşile", "Ulus", "Batman", "Beşiri", "Gercüş", "Hasankeyf", "Kozluk", "Sason",
				"Aydıntepe", "Bayburt", "Demirözü", "Bilecik", "Bozüyük", "Gölpazarı", "İnhisar", "Osmaneli",
				"Pazaryeri", "Söğüt", "Yenipazar", "Adaklı", "Bingöl", "Genç", "Karlıova", "Kiğı", "Solhan",
				"Yayladere", "Yedisu", "Adilcevaz", "Ahlat", "Bitlis", "Güroymak", "Hizan", "Mutki", "Tatvan", "Bolu",
				"Dörtdivan", "Gerede", "Göynük", "Kıbrıscık", "Mengen", "Mudurnu", "Seben", "Yeniçağa", "Ağlasun",
				"Altınyayla", "Bucak", "Burdur", "Çavdır", "Çeltikçi", "Gölhisar", "Karamanlı", "Kemer", "Tefenni",
				"Yeşilova", "Büyükorhan", "Gemlik", "Gürsu", "Harmancık", "İnegöl", "İznik", "Karacabey", "Keles",
				"Kestel", "Mudanya", "Mustafakemalpaşa", "Nilüfer", "Orhaneli", "Orhangazi", "Osmangazi", "Yenişehir",
				"Yıldırım", "Ayvacık", "Bayramiç", "Biga", "Bozcaada", "Çan", "Çanakkale", "Eceabat", "Ezine",
				"Gelibolu", "Gökçeada", "Lapseki", "Yenice", "Atkaracalar", "Bayramören", "Çankırı", "Çerkeş",
				"Eldivan", "Ilgaz", "Kızılırmak", "Korgun", "Kurşunlu", "Orta", "Şabanözü", "Yapraklı", "Alaca",
				"Bayat", "Boğazkale", "Çorum", "Dodurga", "İskilip", "Kargı", "Laçin", "Mecitözü", "Oğuzlar", "Ortaköy",
				"Osmancık", "Sungurlu", "Uğurludağ", "Acıpayam", "Babadağ", "Baklan", "Bekilli", "Beyağaç", "Bozkurt",
				"Buldan", "Çal", "Çameli", "Çardak", "Çivril", "Güney", "Honaz", "Kale", "Merkezefendi", "Pamukkale",
				"Sarayköy", "Serinhisar", "Tavas", "Bağlar", "Bismil", "Çermik", "Çınar", "Çüngüş", "Dicle", "Eğil",
				"Ergani", "Hani", "Hazro", "Kayapınar", "Kocaköy", "Kulp", "Lice", "Silvan", "Sur", "Yenişehir",
				"Akçakoca", "Cumayeri", "Çilimli", "Düzce", "Gölyaka", "Gümüşova", "Kaynaşlı", "Yığılca", "Enez",
				"Havsa", "İpsala", "Keşan", "Lalapaşa", "Meriç", "Merkez", "Süloğlu", "Uzunköprü", "Ağın", "Alacakaya",
				"Arıcak", "Baskil", "Elâzığ", "Karakoçan", "Keban", "Kovancılar", "Maden", "Palu", "Sivrice", "Çayırlı",
				"Erzincan", "İliç", "Kemah", "Kemaliye", "Otlukbeli", "Refahiye", "Tercan", "Üzümlü", "Aşkale",
				"Aziziye", "Çat", "Hınıs", "Horasan", "İspir", "Karaçoban", "Karayazı", "Köprüköy", "Narman", "Oltu",
				"Olur", "Palandöken", "Pasinler", "Pazaryolu", "Şenkaya", "Tekman", "Tortum", "Uzundere", "Yakutiye",
				"Alpu", "Beylikova", "Çifteler", "Günyüzü", "Han", "İnönü", "Mahmudiye", "Mihalgazi", "Mihalıççık",
				"Odunpazarı", "Sarıcakaya", "Seyitgazi", "Sivrihisar", "Tepebaşı", "Araban", "İslahiye", "Karkamış",
				"Nizip", "Nurdağı", "Oğuzeli", "Şahinbey", "Şehitkâmil", "Yavuzeli", "Alucra", "Bulancak", "Çamoluk",
				"Çanakçı", "Dereli", "Doğankent", "Espiye", "Eynesil", "Giresun", "Görele", "Güce", "Keşap", "Piraziz",
				"Şebinkarahisar", "Tirebolu", "Yağlıdere", "Gümüşhane", "Kelkit", "Köse", "Kürtün", "Şiran", "Torul",
				"Çukurca", "Hakkâri", "Şemdinli", "Yüksekova", "Altınözü", "Antakya", "Arsuz", "Belen", "Defne",
				"Dörtyol", "Erzin", "Hassa", "İskenderun", "Kırıkhan", "Kumlu", "Payas", "Reyhanlı", "Samandağ",
				"Yayladağı", "Aralık", "Iğdır", "Karakoyunlu", "Tuzluca", "Aksu", "Atabey", "Eğirdir", "Gelendost",
				"Gönen", "Isparta", "Keçiborlu", "Senirkent", "Sütçüler", "Şarkikaraağaç", "Uluborlu", "Yalvaç",
				"Yenişarbademli", "Adalar", "Arnavutköy", "Ataşehir", "Avcılar", "Bağcılar", "Bahçelievler", "Bakırköy",
				"Başakşehir", "Bayrampaşa", "Beşiktaş", "Beykoz", "Beylikdüzü", "Beyoğlu", "Büyükçekmece", "Çatalca",
				"Çekmeköy", "Esenler", "Esenyurt", "Eyüp", "Fatih", "Gaziosmanpaşa", "Güngören", "Kadıköy", "Kağıthane",
				"Kartal", "Küçükçekmece", "Maltepe", "Pendik", "Sancaktepe", "Sarıyer", "Silivri", "Sultanbeyli",
				"Sultangazi", "Şile", "Şişli", "Tuzla", "Ümraniye", "Üsküdar", "Zeytinburnu", "Aliağa", "Balçova",
				"Bayındır", "Bayraklı", "Bergama", "Beydağ", "Bornova", "Buca", "Çeşme", "Çiğli", "Dikili", "Foça",
				"Gaziemir", "Güzelbahçe", "Karabağlar", "Karaburun", "Karşıyaka", "Kemalpaşa", "Kınık", "Kiraz",
				"Konak", "Menderes", "Menemen", "Narlıdere", "Ödemiş", "Seferihisar", "Selçuk", "Tire", "Torbalı",
				"Urla", "Afşin", "Andırın", "Çağlayancerit", "Dulkadiroğlu", "Ekinözü", "Elbistan", "Göksun", "Nurhak",
				"Onikişubat", "Pazarcık", "Türkoğlu", "Eflani", "Eskipazar", "Karabük", "Ovacık", "Safranbolu",
				"Yenice", "Ayrancı", "Başyayla", "Ermenek", "Karaman", "Kazımkarabekir", "Sarıveliler", "Akyaka",
				"Arpaçay", "Digor", "Kağızman", "Kars", "Sarıkamış", "Selim", "Susuz", "Abana", "Ağlı", "Araç",
				"Azdavay", "Bozkurt", "Cide", "Çatalzeytin", "Daday", "Devrekani", "Doğanyurt", "Hanönü", "İhsangazi",
				"İnebolu", "Kastamonu", "Küre", "Pınarbaşı", "Seydiler", "Şenpazar", "Taşköprü", "Tosya", "Akkışla",
				"Bünyan", "Develi", "Felahiye", "Hacılar", "İncesu", "Kocasinan", "Melikgazi", "Özvatan", "Pınarbaşı",
				"Sarıoğlan", "Sarız", "Talas", "Tomarza", "Yahyalı", "Yeşilhisar", "Bahşılı", "Balışeyh", "Çelebi",
				"Delice", "Karakeçili", "Keskin", "Kırıkkale", "Sulakyurt", "Yahşihan", "Babaeski", "Demirköy",
				"Kırklareli", "Kofçaz", "Lüleburgaz", "Pehlivanköy", "Pınarhisar", "Vize", "Akçakent", "Akpınar",
				"Boztepe", "Çiçekdağı", "Kaman", "Kırşehir", "Mucur", "Elbeyli", "Kilis", "Musabeyli", "Polateli",
				"Başiskele", "Çayırova", "Darıca", "Derince", "Dilovası", "Gebze", "Gölcük", "İzmit", "Kandıra",
				"Karamürsel", "Kartepe", "Körfez", "Ahırlı", "Akören", "Akşehir", "Altınekin", "Beyşehir", "Bozkır",
				"Cihanbeyli", "Çeltik", "Çumra", "Derbent", "Derebucak", "Doğanhisar", "Emirgazi", "Ereğli",
				"Güneysınır", "Hadım", "Halkapınar", "Hüyük", "Ilgın", "Kadınhanı", "Karapınar", "Karatay", "Kulu",
				"Meram", "Sarayönü", "Selçuklu", "Seydişehir", "Taşkent", "Tuzlukçu", "Yalıhüyük", "Yunak", "Altıntaş",
				"Aslanapa", "Çavdarhisar", "Domaniç", "Dumlupınar", "Emet", "Gediz", "Hisarcık", "Kütahya", "Pazarlar",
				"Şaphane", "Simav", "Tavşanlı", "Akçadağ", "Arapgir", "Arguvan", "Battalgazi", "Darende", "Doğanşehir",
				"Doğanyol", "Hekimhan", "Kale", "Kuluncak", "Pütürge", "Yazıhan", "Yeşilyurt", "Ahmetli", "Akhisar",
				"Alaşehir", "Demirci", "Gölmarmara", "Gördes", "Kırkağaç", "Köprübaşı", "Kula", "Salihli", "Sarıgöl",
				"Saruhanlı", "Selendi", "Soma", "Şehzadeler", "Turgutlu", "Yunusemre", "Artuklu", "Dargeçit", "Derik",
				"Kızıltepe", "Mazıdağı", "Midyat", "Nusaybin", "Ömerli", "Savur", "Yeşilli", "Akdeniz", "Anamur",
				"Aydıncık", "Bozyazı", "Çamlıyayla", "Erdemli", "Gülnar", "Mezitli", "Mut", "Silifke", "Tarsus",
				"Toroslar", "Yenişehir", "Bodrum", "Dalaman", "Datça", "Fethiye", "Kavaklıdere", "Köyceğiz", "Marmaris",
				"Menteşe", "Milas", "Ortaca", "Seydikemer", "Ula", "Yatağan", "Bulanık", "Hasköy", "Korkut",
				"Malazgirt", "Muş", "Varto", "Acıgöl", "Avanos", "Derinkuyu", "Gülşehir", "Hacıbektaş", "Kozaklı",
				"Nevşehir", "Ürgüp", "Altunhisar", "Bor", "Çamardı", "Çiftlik", "Niğde", "Ulukışla", "Akkuş",
				"Altınordu", "Aybastı", "Çamaş", "Çatalpınar", "Çaybaşı", "Fatsa", "Gölköy", "Gülyalı", "Gürgentepe",
				"İkizce", "Kabadüz", "Kabataş", "Korgan", "Kumru", "Mesudiye", "Perşembe", "Ulubey", "Ünye", "Bahçe",
				"Düziçi", "Hasanbeyli", "Kadirli", "Osmaniye", "Sumbas", "Toprakkale", "Ardeşen", "Çamlıhemşin",
				"Çayeli", "Derepazarı", "Fındıklı", "Güneysu", "Hemşin", "İkizdere", "İyidere", "Kalkandere", "Pazar",
				"Rize", "Adapazarı", "Akyazı", "Arifiye", "Erenler", "Ferizli", "Geyve", "Hendek", "Karapürçek",
				"Karasu", "Kaynarca", "Kocaali", "Pamukova", "Sapanca", "Serdivan", "Söğütlü", "Taraklı", "Alaçam",
				"Asarcık", "Atakum", "Ayvacık", "Bafra", "Canik", "Çarşamba", "Havza", "İlkadım", "Kavak", "Ladik",
				"Ondokuzmayıs", "Salıpazarı", "Tekkeköy", "Terme", "Vezirköprü", "Yakakent", "Siirt", "Tillo", "Baykan",
				"Eruh", "Kurtalan", "Pervari", "Şirvan", "Ayancık", "Boyabat", "Dikmen", "Durağan", "Erfelek", "Gerze",
				"Saraydüzü", "Sinop", "Türkeli", "Akıncılar", "Altınyayla", "Divriği", "Doğanşar", "Gemerek", "Gölova",
				"Hafik", "İmranlı", "Kangal", "Koyulhisar", "Sivas", "Suşehri", "Şarkışla", "Ulaş", "Yıldızeli", "Zara",
				"Gürün", "Akçakale", "Birecik", "Bozova", "Ceylanpınar", "Eyyübiye", "Halfeti", "Haliliye", "Harran",
				"Hilvan", "Karaköprü", "Siverek", "Suruç", "Viranşehir", "Beytüşşebap", "Cizre", "Güçlükonak", "İdil",
				"Silopi", "Şırnak", "Uludere", "Çerkezköy", "Çorlu", "Ergene", "Hayrabolu", "Kapaklı", "Malkara",
				"Marmara Ereğlisi", "Muratlı", "Saray", "Süleymanpaşa", "Şarköy", "Almus", "Artova", "Başçiftlik",
				"Erbaa", "Niksar", "Pazar", "Reşadiye", "Sulusaray", "Tokat", "Turhal", "Yeşilyurt", "Zile", "Akçaabat",
				"Araklı", "Arsin", "Beşikdüzü", "Çarşıbaşı", "Çaykara", "Dernekpazarı", "Düzköy", "Hayrat", "Köprübaşı",
				"Maçka", "Of", "Ortahisar", "Sürmene", "Şalpazarı", "Tonya", "Vakfıkebir", "Yomra", "Çemişgezek",
				"Hozat", "Mazgirt", "Nazımiye", "Ovacık", "Pertek", "Pülümür", "Tunceli", "Banaz", "Eşme", "Karahallı",
				"Sivaslı", "Ulubey", "Uşak", "Bahçesaray", "Başkale", "Çaldıran", "Çatak", "Edremit", "Erciş", "Gevaş",
				"Gürpınar", "İpekyolu", "Muradiye", "Özalp", "Saray", "Tuşba", "Altınova", "Armutlu", "Çınarcık",
				"Çiftlikköy", "Termal", "Yalova", "Akdağmadeni", "Aydıncık", "Boğazlıyan", "Çandır", "Çayıralan",
				"Çekerek", "Kadışehri", "Saraykent", "Sarıkaya", "Sorgun", "Şefaatli", "Yenifakılı", "Yerköy", "Yozgat",
				"Alaplı", "Çaycuma", "Devrek", "Gökçebey", "Kilimli", "Kozlu", "Karadeniz Ereğli", "Zonguldak" };
		arrayBanks = new String[] { "Akbank", "Aktifbank", "Albaraka Türk", "Alternatif Bank", "Anadolu Bank",
				"A&T Bank", "Bank Mellat", "Bank Pozitif", "Burgan Bank", "Citibank", "DenizBank", "Deutsche Bank",
				"Fibabanka", "QNB Finansbank", "Garanti Bankası", "GSD Yatırım Bankası", "Halkbank", "HSBC", "ING Bank",
				"J.P. Morgan Chase", "Kuveyt Türk Katılım Bankası", "Merkezi Kayıt Kuruluşu", "Şekerbank",
				"ICBC Turkey", "Türk Ekonomi Bankası", "TURKISHBANK", "Finans Katılım Bankası", "İş Bankası",
				"Sınai Kalkınma Bankası", "VakıfBank", "TURKLANDBANK", "Ziraat Bankası" };

		arrayFirstNames = new String[] { "Abay", "Abbas", "Abdullah", "Abidin", "Acun", "Adem", "Adil", "Adnan",
				"Affan", "Agah", "Ahmet", "Akad", "Akay", "Akel", "Akgün", "Akin", "Akif", "Akil", "Aktaç", "Aktan",
				"Alaaddin", "Aldemir", "Ali", "Alican", "Alim", "Alişan", "Alkan", "Alkin", "Alp", "Alpar", "Alpaslan",
				"Alpay", "Alper", "Alphan", "Alptekin", "Altan", "Altay", "Altuğ", "Andaç", "Anil", "Aral", "Aras",
				"Arcan", "Arda", "Arel", "Argün", "Arikan", "Arin", "Arif", "Arkan", "Armağan", "Arman", "Arslan",
				"Artaç", "Asim", "Asil", "Aslan", "Asrin", "Asutay", "Aşkin", "Ata", "Atabek", "Atabey", "Atacan",
				"Atahan", "Atak", "Atakan", "Atalay", "Ataman", "Atanur", "Atasoy", "Atay", "Ateş", "Atif", "Atil",
				"Atilay", "Atilgan", "Atinç", "Atilla", "Atlihan", "Avni", "Aybar", "Aybars", "Ayberk", "Aydemir",
				"Aydin", "Aygün", "Ayhan", "Aykan", "Aykut", "Aytaç", "Aytek", "Aytekin", "Aytunç", "Ayyüce", "Azer",
				"Azim", "Aziz", "Azmi", "Babür", "Baha", "Bahadir", "Bahattin", "Bahir", "Bahri", "Bahtiyar", "Baki",
				"Bala", "Balaban", "Baler", "Baran", "Baransel", "Barbaros", "Barin", "Bariş", "Barkan", "Barkin",
				"Barlas", "Bars", "Basri", "Başar", "Başer", "Bati", "Batihan", "Batikan", "Batiray", "Battal", "Batu",
				"Batuhan", "Batur", "Baturalp", "Bayar", "Baybars", "Baybora", "Baycan", "Bayezit", "Bayhan", "Baykal",
				"Bayraktar", "Bayram", "Baysal", "Bayülken", "Bedir", "Bedirhan", "Bedrettin", "Bedri", "Behçet",
				"Behiç", "Behlül", "Behram", "Behzat", "Beki", "Bekir", "Bektaş", "Berat", "Berhan", "Berk", "Berkan",
				"Berkant", "Berkay", "Berke", "Bermal", "Besim", "Bilal", "Bilge", "Bilgehan", "Bilgin", "Birant",
				"Bircan", "Birol", "Boğaç", "Boğaçhan", "Bora", "Bozkurt", "Buğra", "Buğrahan", "Bulut", "Bumin",
				"Burak", "Burç", "Burçak", "Burçin", "Burhan", "Burhanettin", "Bülent", "Bünyamin", "Cabbar", "Cafer",
				"Cahit", "Can", "Canalp", "Canberk", "Candaş", "Candemir", "Candoğan", "Canel", "Caner", "Cankat",
				"Cankut", "Cansin", "Cantekin", "Cavit", "Celal", "Celalettin", "Celayir", "Celil", "Cem", "Cemal",
				"Cemalettin", "Cemil", "Cemre", "Cenan", "Cenap", "Cengiz", "Cengizhan", "Cenk", "Cevahir", "Cevat",
				"Cevdet", "Ceyhan", "Ceyhun", "Cezmi", "Cihan", "Cihangir", "Cihat", "Civan", "Coşar", "Coşku",
				"Coşkun", "Cuma", "Cumhur", "Cüneyt", "Çağan", "Çağatay", "Çağdaş", "Çağin", "Çağlar", "Çağman",
				"Çağri", "Çakabey", "Çakar", "Çakin", "Çakir", "Çavuş", "Çelebi", "Çelen", "Çelik", "Çeliker", "Çetin",
				"Çevik", "Çevrim", "Çiğir", "Çinar", "Dalan", "Dalay", "Daniş", "Darcan", "Davut", "Deha", "Demir",
				"Demiralp", "Demircan", "Demirel", "Demirhan", "Demirkan", "Deniz", "Denizhan", "Denktaş", "Derin",
				"Derviş", "Derya", "Devlet", "Devran", "Devrim", "Dilaver", "Dilmen", "Dinç", "Dinçer", "Doğa", "Doğan",
				"Doğu", "Doğuhan", "Doğukan", "Doğuş", "Doruk", "Dorukhan", "Duran", "Durmuş", "Dursun", "Durukan",
				"Durul", "Duyal", "Dündar", "Dünya", "Ecevit", "Edip", "Ediz", "Efdal", "Efe", "Efgan", "Eflatun",
				"Ege", "Egemen", "Ejder", "Ekber", "Ekin", "Ekrem", "Eldem", "Elvan", "Emin", "Emir", "Emirhan",
				"Emrah", "Emre", "Emrullah", "Ender", "Ener", "Engin", "Enginsu", "Enis", "Ensar", "Enver", "Eralp",
				"Eray", "Erbatur", "Erberk", "Ercan", "Ercüment", "Erçin", "Erdal", "Erdem", "Erden", "Erdinç",
				"Erdoğan", "Erem", "Eren", "Erenay", "Ergin", "Ergun", "Ergün", "Erhan", "Erhun", "Erim", "Erinç",
				"Erkal", "Erkan", "Erkin", "Erkut", "Erman", "Erol", "Ersan", "Ersen", "Ersin", "Erşat", "Ertaç",
				"Ertan", "Ertem", "Erten", "Ertuğrul", "Eryaman", "Esat", "Esen", "Eser", "Eşref", "Evgin", "Evren",
				"Evrim", "Eyüp", "Ezel", "Fadil", "Fahir", "Fahrettin", "Fahri", "Faik", "Fakir", "Falih", "Faruk",
				"Fatih", "Fazil", "Fehim", "Ferdi", "Ferhan", "Ferhat", "Ferid", "Feridun", "Ferit", "Ferkan", "Ferruh",
				"Fethi", "Fevzi", "Feyyaz", "Feyzi", "Feyzullah", "Feza", "Firat", "Fikret", "Fikri", "Fuat", "Furkan",
				"Gaffar", "Gafur", "Galip", "Gani", "Garip", "Gazanfer", "Gazi", "Gediz", "Gencal", "Gencalp", "Gencay",
				"Gencer", "Genco", "Giray", "Girgin", "Gökalp", "Gökay", "Gökberk", "Gökcan", "Gökçe", "Gökçen",
				"Göker", "Gökhan", "Gökhun", "Gökmen", "Göksel", "Göktan", "Göktuğ", "Göktürk", "Güçhan", "Güçlü",
				"Gülhan", "Gültekin", "Günalp", "Günay", "Gündoğdu", "Gündüz", "Güner", "Güneri", "Güneş", "Güney",
				"Güngör", "Günhan", "Günsel", "Günser", "Güntan", "Güntekin", "Güral", "Güralp", "Güray", "Gürbüz",
				"Gürcan", "Gürel", "Gürkan", "Gürol", "Gürsel", "Gürsoy", "Gürtan", "Güven", "Güvenç", "Güzey", "Habib",
				"Haci", "Hafiz", "Hakan", "Hakki", "Haldun", "Halil", "Halim", "Halis", "Halit", "Haluk", "Hamdi",
				"Hamdullah", "Hami", "Hamit", "Hamza", "Hanefi", "Harun", "Hasan", "Hasip", "Hasret", "Haşim", "Haşmet",
				"Hatay", "Hatem", "Hayati", "Haydar", "Hayrettin", "Hayri", "Hayrullah", "Hazar", "Hazim", "Heybet",
				"Hifzi", "Hincal", "Hizir", "Hicri", "Hidayet", "Hikmet", "Hilmi", "Himmet", "Hiram", "Hişam", "Hulki",
				"Hulusi", "Hurşit", "Hüdaverdi", "Hüray", "Hürkan", "Hüsam", "Hüsamettin", "Hüseyin", "Hüsmen", "Hüsnü",
				"Hüsrev", "Ildir", "Ildiz", "Ilgar", "Ilgaz", "Işik", "Işikhan", "Işin", "Işitan", "Itri", "İbrahim",
				"İdris", "İhsan", "İlbey", "İlcan", "İlgi", "İlham", "İlhami", "İlhan", "İlkan", "İlkay", "İlkcan",
				"İlke", "İlker", "İlkin", "İltekin", "İlter", "İlyas", "İmdat", "İnal", "İnan", "İnanç", "İnayet",
				"İrfan", "İsa", "İshak", "İskender", "İslam", "İsmail", "İsmet", "İsrafil", "İstemi", "İşcan", "İzzet",
				"Jerfi", "Jiyan", "Kaan", "Kadem", "Kadir", "Kadri", "Kahraman", "Kamber", "Kamer", "Kamil", "Kamuran",
				"Kandemir", "Kaner", "Kaplan", "Karabey", "Karacan", "Karahan", "Karakan", "Karan", "Karanalp",
				"Karatay", "Kartal", "Kartay", "Kasim", "Kaya", "Kayahan", "Kayhan", "Kazim", "Kemal", "Kemalettin",
				"Kenan", "Keramettin", "Kerem", "Keremşah", "Kerim", "Keyhan", "Kiliç", "Kiliçalp", "Kiliçhan", "Kirca",
				"Kirdar", "Kirhan", "Kivanç", "Kivilcim", "Kolçak", "Konur", "Koral", "Koralp", "Koray", "Korcan",
				"Korçak", "Korel", "Korhan", "Korkmaz", "Korkut", "Kortan", "Köker", "Köksal", "Kubat", "Kubilay",
				"Kudret", "Kuntay", "Kunter", "Kurt", "Kurtbey", "Kurtuluş", "Kutan", "Kutay", "Kutbay", "Kuter",
				"Kuthan", "Kutlay", "Kutlu", "Kutsal", "Kutsi", "Kuzey", "Kürşat", "Laçin", "Lami", "Latif", "Lebib",
				"Lema", "Lemi", "Levent", "Lokman", "Lütfi", "Lütfullah", "Lütfü", "Macit", "Mahir", "Mahmut", "Mahsun",
				"Mahzun", "Makbul", "Maksut", "Malik", "Manço", "Mansur", "Mazhar", "Mazlum", "Mecit", "Mecnun",
				"Medeni", "Medet", "Mehmet", "Melih", "Melik", "Memduh", "Memnun", "Menderes", "Mengü", "Mengüç",
				"Mensur", "Meriç", "Merih", "Mert", "Mestan", "Mesut", "Mete", "Metin", "Mevlüt", "Mikail", "Mirkelam",
				"Mirza", "Mithat", "Muammer", "Mucip", "Muhammed", "Muharrem", "Muhip", "Muhittin", "Muhlis", "Muhsin",
				"Muhtar", "Muhteşem", "Mukbil", "Munis", "Murat", "Murathan", "Murtaza", "Musa", "Mustafa", "Muti",
				"Mutlu", "Mutluhan", "Muzaffer", "Mücahit", "Müfit", "Müjdat", "Mükerrem", "Mükremin", "Mümin",
				"Mümtaz", "Münir", "Müren", "Mürsel", "Mürşit", "Müslüm", "Müşfik", "Müştak", "Nabi", "Naci", "Nadi",
				"Nadir", "Nafi", "Nafiz", "Nahit", "Nail", "Naim", "Namik", "Nami", "Nasir", "Nasrettin", "Nasuh",
				"Nasuhi", "Naşit", "Nazim", "Nazir", "Nazif", "Nazmi", "Nebi", "Necat", "Necati", "Necdet", "Necip",
				"Necmettin", "Necmi", "Nedim", "Nedret", "Nehar", "Nejat", "Nesim", "Neşat", "Neşet", "Nevzat",
				"Neyzen", "Nezih", "Nezihi", "Nihat", "Niyazi", "Nizam", "Nizamettin", "Nizami", "Nuh", "Numan",
				"Nurettin", "Nuri", "Nurkan", "Nurşat", "Nurtaç", "Nusret", "Nusrettin", "Nüvit", "Nüzhet", "Oflaz",
				"Ogün", "Oğan", "Oğul", "Oğur", "Oğuz", "Oğuzhan", "Okan", "Okay", "Okcan", "Oker", "Oktar", "Oktay",
				"Olcay", "Olcayto", "Olgun", "Omaç", "Omay", "Onat", "Onay", "Ongar", "Ongun", "Onur", "Onural",
				"Onuralp", "Onurhan", "Orbay", "Orçun", "Orhan", "Orhun", "Orkun", "Orkut", "Ortaç", "Ortun", "Ortunç",
				"Oruç", "Osman", "Oytun", "Ozan", "Öcal", "Ödül", "Ögeday", "Öğün", "Öğünç", "Öğüt", "Öker", "Ökkeş",
				"Ökmen", "Öktem", "Ökten", "Ömer", "Ömür", "Önal", "Önay", "Önder", "Önel", "Öner", "Örsan", "Örsel",
				"Övül", "Övünç", "Öymen", "Özal", "Özalp", "Özay", "Özbek", "Özcan", "Özdemir", "Özden", "Özen", "Özer",
				"Özgün", "Özgür", "Özhan", "Özkan", "Özmen", "Öztürk", "Özün", "Pakel", "Paker", "Paksoy", "Pala",
				"Pamir", "Pars", "Paşa", "Paydaş", "Payidar", "Pehlivan", "Pekcan", "Peker", "Perker", "Pertev",
				"Peyam", "Peyami", "Peykan", "Peyman", "Polat", "Poyraz", "Pozan", "Raci", "Rafet", "Ragip", "Rahim",
				"Rahman", "Rahmi", "Raif", "Rakim", "Ramazan", "Rami", "Ramiz", "Rasim", "Rasin", "Raşit", "Rauf",
				"Recai", "Recep", "Refet", "Refiğ", "Refik", "Reha", "Remzi", "Renan", "Resul", "Reşat", "Reşit",
				"Ridvan", "Rifat", "Rifki", "Riza", "Ruhi", "Ruşen", "Rüçhan", "Rüknettin", "Rüstem", "Rüştü",
				"Saadettin", "Sabah", "Sabahattin", "Sabir", "Sabit", "Sabri", "Sacit", "Sadettin", "Sadik", "Sadri",
				"Sadullah", "Sadun", "Safa", "Saffet", "Safi", "Saim", "Sait", "Sakip", "Sakin", "Salih", "Salim",
				"Saltuk", "Samet", "Sami", "Samih", "Samim", "Sanberk", "Sancar", "Saner", "Sanver", "Sargin", "Sarp",
				"Sarper", "Saruhan", "Savaş", "Sayhan", "Sazak", "Seçkin", "Sedat", "Sefa", "Sefa", "Sefer", "Seha",
				"Selahattin", "Selami", "Selcan", "Selçuk", "Selim", "Selman", "Semih", "Senih", "Seralp", "Serbülent",
				"Sercan", "Serdar", "Sergen", "Serhan", "Serhat", "Serkan", "Serkut", "Sermet", "Sertaç", "Serter",
				"Server", "Servet", "Seyfettin", "Seyfi", "Seyhan", "Seyit", "Sezai", "Sezer", "Sezgin", "Siddik",
				"Sitki", "Simavi", "Sina", "Sinan", "Sipahi", "Soner", "Songur", "Soysal", "Sökmen", "Sönmez", "Suat",
				"Suavi", "Suay", "Suphi", "Süleyman", "Sümer", "Süreyya", "Süruri", "Şaban", "Şadi", "Şafak", "Şahap",
				"Şahin", "Şahzat", "Şair", "Şakir", "Şamil", "Şansal", "Şanver", "Şarik", "Şecaattin", "Şefik",
				"Şehmuz", "Şehzade", "Şemsettin", "Şenel", "Şener", "Şenol", "Şensoy", "Şentürk", "Şerafettin", "Şeref",
				"Şerif", "Şevket", "Şevki", "Şinasi", "Şükrü", "Taci", "Taçkin", "Tahir", "Tahsin", "Taki", "Talat",
				"Talay", "Talip", "Tamay", "Tamer", "Tan", "Tanay", "Tanberk", "Taner", "Tanju", "Tankut", "Tansel",
				"Tarhan", "Tarik", "Tarkan", "Taşkin", "Tayfun", "Tayfur", "Taygun", "Taylan", "Tayyar", "Tayyib",
				"Tekcan", "Tekin", "Temel", "Teoman", "Tercan", "Tevfik", "Tezalp", "Tezcan", "Tezkan", "Tinaz",
				"Timuçin", "Timur", "Tokcan", "Toker", "Toktamiş", "Tolga", "Tolunay", "Tonguç", "Toprak", "Toygar",
				"Tufan", "Tugay", "Tuğrul", "Tuna", "Tunca", "Tuncay", "Tuncel", "Tuncer", "Tunç", "Turaç", "Turan",
				"Turgay", "Turgut", "Turhan", "Tümay", "Türkay", "Türker", "Türkeş", "Uçar", "Uçhan", "Uçkan", "Ufuk",
				"Uğur", "Uğuralp", "Uğurcan", "Ulaç", "Ulaş", "Uluç", "Ulunay", "Ulvi", "Umur", "Umut", "Ural", "Utkan",
				"Utku", "Uygur", "Uzay", "Uzel", "Uzer", "Ülgen", "Ülkem", "Ülker", "Ümit", "Ünal", "Ünalp", "Ünay",
				"Üner", "Ünkan", "Ünsal", "Ünverdi", "Üstay", "Üster", "Üzeyir", "Vafit", "Vahap", "Vahdet", "Vahdi",
				"Vahit", "Vakur", "Varol", "Vasfi", "Vecdi", "Vecihi", "Vedat", "Vefa", "Vefi", "Vefik", "Vehbi",
				"Veli", "Velit", "Veysel", "Veysi", "Volkan", "Vural", "Yağiz", "Yahya", "Yakup", "Yakut", "Yalaz",
				"Yalçin", "Yalim", "Yalin", "Yalinay", "Yalman", "Yamaç", "Yaman", "Yasin", "Yaşar", "Yavuz", "Yazgan",
				"Yekta", "Yenal", "Yener", "Yetkin", "Yildiray", "Yildirim", "Yilmaz", "Yiğit", "Yordam", "Yunus",
				"Yurdaer", "Yurdakul", "Yurtcan", "Yusuf", "Yücel", "Yüksel", "Yadigar", "Zafer", "Zahir", "Zahit",
				"Zekai", "Zekeriya", "Zeki", "Zeycan", "Zeynel", "Zihni", "Zikri", "Zirve", "Ziya", "Zorlu", "Zühtü" };
		arrayLastNames = new String[] { "Abacı", "Abadan", "Aclan", "Adal", "Adan", "Adem", "Adıvar", "Akal", "Akan",
				"Akar", "Akay", "Akaydın", "Akbulut", "Akgül", "Akışık", "Akman", "Akyürek", "Akyüz", "Akşit",
				"Alnıaçık", "Alpuğan", "Alyanak", "Arıcan", "Arslanoğlu", "Atakol", "Atan", "Avan", "Ayaydın", "Aybar",
				"Aydan", "Aykaç", "Ayverdi", "Ağaoğlu", "Aşıkoğlu", "Babacan", "Babaoğlu", "Bademci", "Bakırcıoğlu",
				"Balaban", "Balcı", "Barbarosoğlu", "Baturalp", "Baykam", "Başoğlu", "Berberoğlu", "Beşerler", "Beşok",
				"Biçer", "Bolatlı", "Dalkıran", "Dağdaş", "Dağlaroğlu", "Demirbaş", "Demirel", "Denkel", "Dizdar",
				"Doğan", "Durak", "Durmaz", "Duygulu", "Düşenkalkar", "Egeli", "Ekici", "Ekşioğlu", "Eliçin",
				"Elmastaşoğlu", "Erbay", "Erberk", "Erbulak", "Erdoğan", "Erez", "Erginsoy", "Erkekli", "Eronat",
				"Ertepınar", "Ertürk", "Erçetin", "Evliyaoğlu", "Fahri", "Gönültaş", "Gümüşpala", "Günday", "Gürmen",
				"Hakyemez", "Hamzaoğlu", "Ilıcalı", "Kahveci", "Kaplangı", "Karabulut", "Karaböcek", "Karadaş",
				"Karaduman", "Karaer", "Kasapoğlu", "Kavaklıoğlu", "Kaya", "Keseroğlu", "Keçeci", "Kılıççı", "Kıraç)",
				"Kocabıyık", "Korol", "Koyuncu", "Koç", "Koçyiğit", "Kuday", "Kulaksızoğlu", "Kumcuoğlu", "Kunter",
				"Kurutluoğlu", "Kutlay", "Kuzucu", "Körmükçü", "Köybaşı", "Köylüoğlu", "Küçükler", "Limoncuoğlu",
				"Mayhoş", "Menemencioğlu", "Mertoğlu", "Nalbantoğlu", "Nebioğlu", "Numanoğlu", "Okumuş", "Okur",
				"Oraloğlu", "Ozansoy", "Paksüt", "Pekkan", "Pektemek", "Polat", "Poyrazoğlu", "Poçan", "Saatçi",
				"Sadıklar", "Samancı", "Sandalcı", "Sarıoğlu", "Saygıner", "Sepetçi", "Sezek", "Sinanoğlu", "Solmaz",
				"Sözeri", "Süleymanoğlu", "Tanrıkulu", "Tazegül", "Taşlı", "Taşçı", "Tekand", "Tekelioğlu",
				"Tokatlıoğlu", "Tokgöz", "Topaloğlu", "Topçuoğlu", "Toraman", "Tunaboylu", "Tunçeri", "Tuğlu", "Tuğluk",
				"Türkdoğan", "Türkyılmaz", "Tütüncü", "Tüzün", "Uca", "Velioğlu", "Yalçın", "Yazıcı", "Yetkiner",
				"Yeşilkaya", "Yıldırım", "Yıldızoğlu", "Yılmazer", "Yorulmaz", "Çamdalı", "Çapanoğlu", "Çatalbaş",
				"Çağıran", "Çetin", "Çetiner", "Çevik", "Çörekçi", "Önür", "Örge", "Öymen", "Özberk", "Özbey", "Özbir",
				"Özdenak", "Özdoğan", "Özkara", "Özkök", "Öztonga", "Öztuna", "Öztürk", "Özyiğit", "Üstünel", "Üvez",
				"Üzümcü", "Üçtaş" };
	}
	private static String[] arrayLastNames;
	private static String[] arrayFirstNames;
	private static String[] arrayBanks;
	private static String[] arrayTowns;

	public Jared() {

	}
	
	public static long genLongNumber(int minLen, int maxLen){
		if (maxLen > 19)
			maxLen = 19;
		if (minLen < 1)
			minLen = 1;
		
		int len;
		
		if (minLen >= maxLen)
			len = Math.min(minLen, maxLen);
		else
			len = ThreadLocalRandom.current().nextInt(minLen, maxLen + 1);
		long origin = 1;
		long bound = 9;
		for(int i=0; i<len-1;i++) {
			origin = origin * 10;
			bound = bound * 10 + 9;
		}
		
		return ThreadLocalRandom.current().nextLong(origin, bound);
	}
	
	public static long genLongNumber(int len){
		return genLongNumber(len, len);
	}
	
	public static long genLongNumber(){
		return genLongNumber(10);
	}
	
	public static int genIntNumber(int minLen, int maxLen){
		int maxIntLen = String.valueOf(Integer.MAX_VALUE).length();
		maxLen = Math.min(maxIntLen-1, maxLen);
		int result = (int)genLongNumber(minLen, maxLen);	
		return result;
	}
	
	public static int genIntNumber(int len){
		return genIntNumber(len, len);
	}
	
	public static int genIntNumber(){
		return genIntNumber(10);
	}
	
	public static String genTown(){
		int len = arrayTowns.length;
		return arrayTowns[ThreadLocalRandom.current().nextInt(len)];
	}

	public static LocalDate genLocalDate() {
		return genLocalDate(2000, 1, 1, 2017, 1, 1);
	}

	public static LocalDate genLocalDate(int fromYear, int fromMonth, int fromDay, int toYear, int toMonth, int toDay) {
		int minDay = (int) LocalDate.of(fromYear, fromMonth, fromDay).toEpochDay();
		int maxDay = (int) LocalDate.of(toYear, toMonth, toDay).toEpochDay();
		long randomDay = minDay + ThreadLocalRandom.current().nextInt(maxDay - minDay);

		LocalDate randomDate = LocalDate.ofEpochDay(randomDay);

		return randomDate;
	}
	
	public static LocalTime genLocalTime() {
		int hour = ThreadLocalRandom.current().nextInt(24);
		int minute = ThreadLocalRandom.current().nextInt(60);
		int second = ThreadLocalRandom.current().nextInt(60);
		return LocalTime.of(hour, minute, second);
	}

	public static LocalDateTime genLocalDateTime() {
		return LocalDateTime.of(genLocalDate(), genLocalTime());
	}
	
	public static String genCustomList(String... words) {
		int len = words.length;
		return words[ThreadLocalRandom.current().nextInt(len)];
	}

	public static long genTCKimlik() {
		return genTCKimlik(ThreadLocalRandom.current().nextInt(8)+1);
	}
	
	public static long genTCKimlik(int startsWith) {
		int len = String.valueOf(startsWith).length();
		
		if (len > 7 || startsWith < 1)
			startsWith = 7;
		
		int tcKimlik = startsWith * (int)(Math.pow(10, 9-len));
		
		tcKimlik += genIntNumber(9-len);
		
		int parity1;
		int parity2;

		int[] digits = Utilz.getDigits(tcKimlik);
		parity1 = (7 * (digits[0] + digits[2] + digits[4] + digits[6] + digits[8])
				- (digits[1] + digits[3] + digits[5] + digits[7])) % 10;
		parity2 = (digits[0] + digits[1] + digits[2] + digits[3] + digits[4] + digits[5] + digits[6] + digits[7]
				+ digits[8] + parity1) % 10;

		long result = (long) 100 * tcKimlik + 10 * parity1 + 1 * parity2;

		return result;
	}

	public static String genFirstName() {
		return arrayFirstNames[ThreadLocalRandom.current().nextInt(arrayFirstNames.length)];
	}

	public static String genLastName() {
		return arrayLastNames[ThreadLocalRandom.current().nextInt(arrayLastNames.length)];
	}

	public static String genFullName() {
		return genFirstName() + " " + genLastName();
	}

	public static String genEmail() {
		String firstName = genFirstName();
		String lastName = genLastName();

		return genEmail(firstName, lastName);
	}

	public static String genEmail(String firstName, String lastName) {
		String name = Utilz.tr2eng(firstName).toLowerCase();
		String surname = Utilz.tr2eng(lastName).toLowerCase();

		String[] postfix = { "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
				"", "21", "22", "23", "24", "25", "80", "81", "82", "83", "84", "85", "88", "90", "2000", "3000" };
		String[] middle = { "", "", "", "", "", "", ".", "_", "-" };
		String[] domain = { "gmail.com", "hotmail.com", "yahoo.com", "mynet.com", "gib.gov.tr", "usa.net" };
		
		if (ThreadLocalRandom.current().nextInt(100) <30)
			name = name.charAt(0) + "";

		return String.format("%s%s%s%s@%s", name, middle[ThreadLocalRandom.current().nextInt(middle.length)], surname,
				postfix[ThreadLocalRandom.current().nextInt(postfix.length)],
				domain[ThreadLocalRandom.current().nextInt(domain.length)]);
	}
	
	public static Tuple<Integer, String> genBank() {
		int len = arrayBanks.length;
		int random = ThreadLocalRandom.current().nextInt(len);

		Tuple<Integer, String> bank = new Tuple<>(random + 101, arrayBanks[random]);

		return bank;
	}

	public static String genMobileNumber() {
		String[] opCodes = { "530", "531", "532", "533", "534", "535", "536", "537", "538", "541", "542", "543", "544",
				"505", "507", "551", "552", "555" };
		String opCode = opCodes[ThreadLocalRandom.current().nextInt(opCodes.length)].toString();
		String num1 = String.valueOf((100 + ThreadLocalRandom.current().nextInt(899)));
		String num2 = String.valueOf((10 + ThreadLocalRandom.current().nextInt(89)));
		String num3 = String.valueOf((10 + ThreadLocalRandom.current().nextInt(89)));

		return String.format("0 %s %s %s", opCode, num1, num2, num3);
	}

	public static String genLandLineNumber() {
		String[] opCodes = { "212", "216", "312", "412", "424", "442" };
		String opCode = opCodes[ThreadLocalRandom.current().nextInt(opCodes.length)].toString();
		String num1 = String.valueOf((100 + ThreadLocalRandom.current().nextInt(899)));
		String num2 = String.valueOf((10 + ThreadLocalRandom.current().nextInt(89)));
		String num3 = String.valueOf((10 + ThreadLocalRandom.current().nextInt(89)));

		return String.format("0 %s %s %s", opCode, num1, num2, num3);
	}

	public static String genPhoneNumber() {
		int random = ThreadLocalRandom.current().nextInt(2);
		if (random == 0)
			return genMobileNumber();
		else
			return genLandLineNumber();
	}

	public static String genFromLookup() {

		return null;

	}

	public static String genIBAN(){
		//"TR93 0006 4000 0011 2341 2345 67"
		long num1 = genLongNumber(12);
		long num2 = genLongNumber(12);
		
		return "TR" +String.valueOf(num1)+String.valueOf(num2);
	}

}
