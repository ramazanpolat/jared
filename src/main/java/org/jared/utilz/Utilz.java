package org.jared.utilz;

import java.util.concurrent.ThreadLocalRandom;

public class Utilz {
	
	public static int chooseWithChance(int... args) {
		/*
		 * This method takes number of probabilities and randomly chooses one of
		 * them considering their chance to be chosen.
		 */
		int argCount = args.length;
		int sumOfChances = 0;

		for (int i = 0; i < argCount; i++) {
			sumOfChances += args[i];
		}

		int rnd2 = ThreadLocalRandom.current().nextInt(sumOfChances);

		while ((rnd2 -= args[argCount - 1]) > 0) {
			argCount--;
			sumOfChances -= args[argCount - 1];
		}

		return argCount - 1;
	}

	public static int[] getDigits(int number) {
		String numberAsString = Integer.toString(number);
		int[] result = new int[numberAsString.length()];
		for (int i = 0; i < result.length; i++) {
			result[i] = Integer.parseInt("" + numberAsString.charAt(i));
		}

		return result;
	}

	public static String tr2eng(String text) {
		String[] olds = { "Ğ", "ğ", "Ü", "ü", "Ş", "ş", "İ", "ı", "Ö", "ö", "Ç", "ç" };
		String[] news = { "G", "g", "U", "u", "S", "s", "I", "i", "O", "o", "C", "c" };

		for (int i = 0; i < olds.length; i++) {
			text = text.replace(olds[i], news[i]);
		}

		// text = text.ToUpper();

		return text;
	}

}
